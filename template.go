package main

import (
	"bytes"
	"fmt"
	log "github.com/sirupsen/logrus"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"text/template"
)

func init() {
	log.SetReportCaller(false)
}

const (
	TemplateDir      = "templates"
	TemplateFile     = ".template"
	PipelineTemplate = "templates/.gitlab-ci.yml"
)

type DockerImage struct {
	Language string
	Version  string
}

func (d DockerImage) Image() string {
	return fmt.Sprintf("%s:%s", d.Language, d.Version)
}

func (d DockerImage) StageName() string {
	return fmt.Sprintf("%s-%s", d.Language, d.Version)
}

func (d DockerImage) DockerfilePath() string {
	return fmt.Sprintf("%s/Dockerfile", d.ImageContext())
}

func (d DockerImage) ImageContext() string {
	return fmt.Sprintf("%s/%s", d.Language, d.Version)
}

func readTemplate(path string) string {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	if len(content) == 0 {
		log.WithFields(
			log.Fields{
				"file_path": path,
			}).Fatalf("template is empty")
	}
	return string(content)
}

func templateLanguage(tpl template.Template, data interface{}) string {
	var tplStr bytes.Buffer
	err := tpl.Execute(&tplStr, data)
	if err != nil {
		log.Fatal(err)
	}
	//fmt.Println(tplStr.String())
	//fmt.Println("---")
	return tplStr.String()
}

func writeTemplate(filePath, template string) {
	f, err := os.Create(filePath)
	if err != nil {
		log.Error(err)
		return
	}
	defer f.Close()

	_, err = f.WriteString(template)
	if err != nil {
		log.Error(err)
		return
	}
}

func copyAdditionalFiles(language, targetPath string) {
	files := make([]string, 0)
	src := fmt.Sprintf("%s/%s", TemplateDir, language)
	err := filepath.Walk(src, func(path string, info fs.FileInfo, err error) error {
		if info.Name() != TemplateFile && !info.IsDir() {
			files = append(files, path)

			//Read all the contents of the  original file
			bytesRead, err := ioutil.ReadFile(path)
			if err != nil {
				log.Fatal(err)
			}

			//Copy all the contents to the desitination file
			destFile := fmt.Sprintf("%s/%s", targetPath, info.Name())
			err = ioutil.WriteFile(destFile, bytesRead, 0755)
			if err != nil {
				log.Fatal(err)
			}
			log.WithFields(
				log.Fields{
					"language":    language,
					"target_path": targetPath,
					"file_name":   info.Name(),
					"target_file": destFile,
				}).Info("copied file")

		}
		return nil
	})
	if err != nil {
		log.Error(err)
	}
}

func main() {

	cfg := map[string][]string{
		"python": []string{
			"3.10-slim-buster",
			"3.11-slim-buster",
			"3-slim-buster",
		},
		"golang": []string{
			"1.19-buster",
			"1.20-buster",
		},
		"docker": []string{
			"23.0",
			"latest",
		},
	}
	//fmt.Println(cfg)
	//_ = readTemplate("python")

	dockerImages := make([]DockerImage, 0)
	for language, versions := range cfg {
		fileName := fmt.Sprintf("%s/%s/%s", TemplateDir, language, TemplateFile)
		tpl := readTemplate(fileName)
		t, err := template.New(language).Parse(tpl)
		if err != nil {
			log.Fatal(err)
		}

		for _, version := range versions {
			di := DockerImage{
				Language: language,
				Version:  version,
			}
			templatedImage := templateLanguage(*t, di)
			if templatedImage == "" {
				log.Errorf("template is empty")
			}
			path := fmt.Sprintf("%s/%s", language, version)
			err = os.MkdirAll(path, os.ModePerm)
			if err != nil {
				log.Error(err)
				continue
			}
			//log.Info(templatedImage)
			writeTemplate(fmt.Sprintf("%s/Dockerfile", path), templatedImage)
			copyAdditionalFiles(language, path)
			dockerImages = append(dockerImages, di)
		}
	}
	tpl := readTemplate(PipelineTemplate)
	t, err := template.New(PipelineTemplate).Parse(tpl)
	if err != nil {
		log.Fatal(err)
	}
	templatedPipeline := templateLanguage(*t, struct {
		Items []DockerImage
	}{
		Items: dockerImages,
	})
	writeTemplate(".gitlab-ci.yml", templatedPipeline)
	log.Info("done.")

	// here write .gitlab.yml
}
