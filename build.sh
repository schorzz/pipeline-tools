#!/bin/bash

REGISTRY=${CI_REGISTRY_IMAGE:-"gitlab-registry"}

function find_dockerfile() {
    echo "start..."
    for filename in $(find ./ -type f -name "Dockerfile"); do
      echo
      dirPath="${filename%/*}"
      directory="${dirPath#*/}"
      language="${directory%/*}"
      tag="${directory#*/}"
      image="${REGISTRY}/${language}:${tag}"
#      echo "docker build -f ${dirPath}/Dockerfile -t ${image} ${dirPath}"
      docker build -f "${dirPath}/Dockerfile" -t "${image}" "${dirPath}"
      docker push "${image}"
      echo
    done
    echo "...done"
}

function main {
    find_dockerfile
}
main "$@"
