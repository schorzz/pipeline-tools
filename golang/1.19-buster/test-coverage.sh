#!/bin/bash

go-acc $(go list ./pkg/...) -o cover.txt -- -v | tee tests.report
cat tests.report | go-junit-report > report.xml
go tool cover -func cover.txt
